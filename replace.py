import os.path
import re
import sys
import time
import subprocess


def runcmd(command):
    ret = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="gbk", timeout=1)
    if ret.returncode == 0:
        print("success:", ret)
    else:
        print("error:", ret)
        sys.exit(0)


images = {}
time_stamp = str(int(time.time())) + "_"


def trans(s):
    gitee_path = "https://gitee.com/mr_zhao_cn/integration/raw/master/images"
    pattern = r"!\[.*?\]\(.*?\)"
    regex = re.compile(pattern)
    result = regex.findall(s)
    if result:
        tmp_str = result[0].replace("file:///", "").replace("/","\\")
        tmp_arr = tmp_str.split("(")
        prifix = tmp_arr[0]
        local_path = tmp_arr[1][:-1]
        file_name = os.path.basename(local_path)
        dir_name = os.path.dirname(local_path)
        save_path = os.path.join(os.getcwd(), time_stamp + "\\" + time_stamp + file_name)
        images[file_name] = local_path, save_path
        replace_value = prifix + "(" + gitee_path + "/" + time_stamp + file_name + ")"
        return replace_value
    else:
        print(s)
        return s


def save_image():
    runcmd(f"mkdir {time_stamp}")
    for item in images.keys():
        print(images[item][0])
        print(images[item][1])
        runcmd(f"copy /-y {images[item][0]} {images[item][1]}")


def main():
    file_content = ""
    with open(file, "r", encoding="utf-8") as f:
        line = f.readline()
        while line:
            result = trans(line)
            file_content += result
            line = f.readline()

    file_save_path = os.path.join(os.getcwd(), os.path.basename(file)) + ".replace"
    save_image()

    print(file_content)
    with open(file_save_path, "w",encoding="utf-8") as f:
        f.write(file_content)


if __name__ == '__main__':
    try:
        file = sys.argv[1]
        print(file)
        sys.exit(main())
    except Exception as e:
        print(e)
        print("请先输入md文件路径")
