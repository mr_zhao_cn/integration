set dir=hap


hdc_std target mount
hdc_std shell mount -o remount,rw /
hdc_std shell rm -rf /data/*

hdc_std shell rm -rf /system/app/com.ohos.systemui/SystemUI*.hap

hdc_std file send %dir%\SystemUI.hap /system/app/com.ohos.systemui/SystemUI.hap
hdc_std file send %dir%\SystemUI-StatusBar.hap /system/app/com.ohos.systemui/SystemUI-StatusBar.hap
hdc_std file send %dir%\SystemUI-NavigationBar.hap /system/app/com.ohos.systemui/SystemUI-NavigationBar.hap
hdc_std file send %dir%\SystemUI-NotificationManagement.hap /system/app/com.ohos.systemui/SystemUI-NotificationManagement.hap
hdc_std file send %dir%\SystemUI-VolumePanel.hap /system/app/com.ohos.systemui/SystemUI-VolumePanel.hap
hdc_std file send %dir%\SystemUI-DropdownPanel.hap /system/app/com.ohos.systemui/SystemUI-DropdownPanel.hap
:: hdc_std file send %dir%\SystemUI-SystemDialog.hap /system/app/com.ohos.systemui/SystemUI-SystemDialog.hap
hdc_std file send %dir%\SystemUI-ScreenLock.hap /system/app/com.ohos.systemui/SystemUI-ScreenLock.hap


pause

hdc_std shell chown root:root /system/app/com.ohos.systemui/SystemUI.hap
hdc_std shell setenforce 0
hdc_std shell sync
hdc_std shell "ls -l /system/app/com.ohos.systemui |grep SystemUI"
hdc_std shell sync /system/bin/udevadm trigger
hdc_std shell reboot
