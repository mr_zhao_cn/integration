echo start; export PATH=/home/tools/oh-command-line-tools/ohpm/bin:${PATH}
export NODE_HOME=/home/tools/node-v16.20.2-linux-x64/bin
# ohpm对应的本地目录：export PATH=/home/zhaohang/openharmony_master/prebuilts/build-tools/common/oh-command-line-tools/ohpm/bin:${PATH}
env
set +e
rm -rf $(pwd)/prebuilts/ohos-sdk/linux 
pushd $(pwd)/prebuilts
supported_sdk_version=('4.0-release' '4.1-release')

for sdk_version in ${supported_sdk_version[@]}; do
    echo $sdk_version
    files=( $(ls -t *.gz | grep -i ${sdk_version} 2>/dev/null) )
    if [ ${#files[@]} -gt 1 ]; then
        newest_file=${files[0]}
        for ((i=1; i<${#files[@]}; i++)); do
            rm -f ${files[i]}
        done
        echo keep the newest file: ${newest_file}
    else
        echo no file need to clean
    fi
done
popd
set -e
python3 $(pwd)/build/scripts/download_sdk.py --branch OpenHarmony-4.0-Release --product-name ohos-sdk-full_4.0-release --api-version 10
python3 $(pwd)/build/scripts/download_sdk.py --branch OpenHarmony-4.1-Release --product-name ohos-sdk-full_4.1-Release --api-version 11
cat > cmds <<EOF
./applications/standard/hap/build.sh --project=$(pwd)/applications/standard/systemui --build_sdk=true
EOF
sh cmds

ps:
1、编译hap包的命令如下（这里以编译systemui举例）：
./applications/standard/hap/build.sh --project=$(pwd)/applications/standard/systemui --build_sdk=true

2、初次需要执行整个脚本，下载sdk，后续只需要执行编译hap包命令，此脚本是参考门禁构建hap包的流水线而得来
3、生成hap后，执行脚本来修改hap包的名称，推送到开发板并重启
