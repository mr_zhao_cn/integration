import json

import jsonpath


def get_components_set(file):
    with open(file) as f:
        data = json.load(f)
        res = set(jsonpath.jsonpath(data, "$..component"))
    return res


def get_components_dict(file):
    name_list = get_components_set(file)
    res = {}
    for item in name_list:
        res[item] = []

    with open(file) as f:
        data = json.load(f)
        for subsystem in data["subsystems"]:
            for component in subsystem["components"]:
                component_name = component["component"]
                try:
                    component_feature = component["features"]
                    res[component_name].extend(component_feature)
                    res[component_name] = list(set(res[component_name]))
                except Exception as e:
                    pass
                    # print(component_name)
    return res


rk_all = get_components_set("rk.json")
pc_all = get_components_set("pc.json")
default_all = get_components_set("default.json")
tablet_all = get_components_set("tablet.json")

rk_unique = rk_all.difference(default_all)
default_unique = default_all.difference(rk_all)

default_rk_common = default_all.intersection(rk_all)

pc_rk_common = pc_all.intersection(rk_all)

tablet_rk_common = tablet_all.intersection(rk_all)


rk_dict = get_components_dict("rk.json")
pc_dict = get_components_dict("pc.json")
default_dict = get_components_dict("default.json")
tablet_dict = get_components_dict("tablet.json")

for item in pc_rk_common:
    if rk_dict[item] != pc_dict[item]:
        print(item,pc_dict[item],"---->",rk_dict[item])


