根据蓝区门禁看板（https://ci.openharmony.cn/workbench/cicd/dailybuild/componentCheck）可知，libunwind需整改内容如下：

![3cca02c2-cf09-4b6e-aabf-f29c23dbf25f](https://gitee.com/mr_zhao_cn/integration/raw/master/images/1709015850_3cca02c2-cf09-4b6e-aabf-f29c23dbf25f.png)

涉及到规则14：InnerAPI必须在bundle.json中描述。本部件模块如果被其他部件依赖，那么检查该模块在本部件bundle.json的inner_kits字段中是否有描述，如果没有则要加上去。

根据看板扫描结果，以下3个模块需要在bundle.json的inner_kits字段中添加描述：

![ae50c6f5-2ac6-4f46-bcf9-e2994fc76378](https://gitee.com/mr_zhao_cn/integration/raw/master/images/1709015850_ae50c6f5-2ac6-4f46-bcf9-e2994fc76378.png)
![75d19b64-c67f-4b94-900d-816ae8e59131](https://gitee.com/mr_zhao_cn/integration/raw/master/images/1709015850_75d19b64-c67f-4b94-900d-816ae8e59131.png)

在 inner_kits 中添加描述：

![808011ab-39f2-4667-850a-ce28bd6a0066](https://gitee.com/mr_zhao_cn/integration/raw/master/images/1709015850_808011ab-39f2-4667-850a-ce28bd6a0066.png)

涉及到规则1：InnerAPI 不能为 source_set，整改方式，将 source_set 目标修改为 ohos_static_library 目标，把源码依赖变更为静态库依赖，静态库依赖作为部件的 InnerAPI 供其他部件调用；

![aa256fde-e535-405d-a409-f098face61e9](https://gitee.com/mr_zhao_cn/integration/raw/master/images/1709015850_aa256fde-e535-405d-a409-f098face61e9.png)
![dc017b73-aaca-4624-87e6-703cbc32e1b9](https://gitee.com/mr_zhao_cn/integration/raw/master/images/1709015850_dc017b73-aaca-4624-87e6-703cbc32e1b9.png)

把source_set类型修改为ohos_static_library静态库：

![454d7a67-24f9-4d19-a86f-64a98cf24560](https://gitee.com/mr_zhao_cn/integration/raw/master/images/1709015850_454d7a67-24f9-4d19-a86f-64a98cf24560.png)


问题：

        本地修改验证通过后，把代码上库之后门禁通过（[libunwind独立编译整改 · Pull Request !129 · OpenHarmony/third_party_libunwind - Gitee.com](https://gitee.com/openharmony/third_party_libunwind/pulls/129)），合入主干；但是导致其他人提交的pr跑门禁失败，现已回退。

报错信息如下：

![49826fb4-3f16-4ebd-8991-acc0198b4b1d](https://gitee.com/mr_zhao_cn/integration/raw/master/images/1709015850_49826fb4-3f16-4ebd-8991-acc0198b4b1d.png)