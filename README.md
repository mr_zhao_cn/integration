该仓用于存储蓝区部件独立编译整改相关的资料

##### 文件夹说明

images:存储md文档所需的图片

其他文件夹:存储md格式的文档



##### 上传文档操作说明

1、首先在本地写好md文档

2、在本地新建一个不带空格的目录（最好是英文）

3、将replace.py、run.bat复制到目录中(windows平台，需要事先下载好python3)

4、双击bat，输入要上传的md文档的本地路径

此时就在目录下生成images文件夹和修正图片路径后的md文档

![image-20240227124141574](https://gitee.com/mr_zhao_cn/integration/raw/master/images/1709008980_image-20240227124141574.png) 

5、将images文件夹中的图片复制到仓库的images文件夹中，将修正后的md文档复制到所需上传的路径中

6、git提交，并推送





